module Lisp


  def self.named_let_stack
    @@named_let_stack ||= Array.new
  end

  
  class Initializer

    def self.initialize_global_environment
      $named_let_stack = []
      Lisp::EnvironmentFrame.global.bind(Symbol.named("nil"), nil)
    end

    def self.register_builtins
      Lisp::PrimEquivalence.register
      Lisp::PrimMath.register
      Lisp::PrimLogical.register
      Lisp::PrimSpecialForms.register
      Lisp::PrimListSupport.register
      Lisp::PrimRelational.register
      Lisp::PrimTypeChecks.register
      Lisp::PrimAssignment.register
      Lisp::PrimIo.register
      Lisp::PrimAlist.register
      Lisp::PrimFrame.register
      Lisp::PrimCharacter.register
      Lisp::PrimString.register
      Lisp::PrimNativeObject.register
      Lisp::PrimClassObject.register
      Lisp::PrimSystem.register
      Lisp::PrimVector.register
      Lisp::Debug.register
      Lisp::PrimEnvironment.register
    end

  end

end
