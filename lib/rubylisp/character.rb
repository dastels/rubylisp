# -*- coding: utf-8 -*-
module Lisp

  class Character < Atom

    def initialize(n)
      @value = n
    end


    def set!(n)
      @value = n
    end


    def character?
      true
    end

    
    def eqv?(other)
      return false unless other.character?
      @value == other.value
    end


    def type
      :character
    end


    def to_s
      @value
    end


    def to_sym
      @value.to_sym
    end


    def find_charactername
      @@character_constants.each {|k, v| return k if v == self}
      "UNKNOWN"
    end


    def print_string
      return "#\\#{find_charactername}"
    end


    @@character_constants = {}
    @@character_constants["altmode"]   = Lisp::Character.new("\e")
    @@character_constants["backnext"]  = Lisp::Character.new("\x1F")
    @@character_constants["backspace"] = Lisp::Character.new("\b")
    @@character_constants["call"]      = Lisp::Character.new("\x1A")
    @@character_constants["linefeed"]  = Lisp::Character.new("\n")
    @@character_constants["newline"]   = Lisp::Character.new("\n")
    @@character_constants["page"]      = Lisp::Character.new("\f")
    @@character_constants["return"]    = Lisp::Character.new("\r")
    @@character_constants["rubout"]    = Lisp::Character.new("\x7F")
    @@character_constants["space"]     = Lisp::Character.new(" ")
    @@character_constants["tab"]       = Lisp::Character.new("\t")
    @@character_constants["NUL"]       = Lisp::Character.new("\x00")
    @@character_constants["SOH"]       = Lisp::Character.new("\x01")
    @@character_constants["STX"]       = Lisp::Character.new("\x02")
    @@character_constants["ETX"]       = Lisp::Character.new("\x03")
    @@character_constants["EOT"]       = Lisp::Character.new("\x04")
    @@character_constants["ENQ"]       = Lisp::Character.new("\x05")
    @@character_constants["ACK"]       = Lisp::Character.new("\x06")
    @@character_constants["BEL"]       = Lisp::Character.new("\x07")
    @@character_constants["BS"]        = Lisp::Character.new("\x08")
    @@character_constants["HT"]        = Lisp::Character.new("\x09")
    @@character_constants["LF"]        = Lisp::Character.new("\x0A")
    @@character_constants["VT"]        = Lisp::Character.new("\x0B")
    @@character_constants["FF"]        = Lisp::Character.new("\x0C")
    @@character_constants["CR"]        = Lisp::Character.new("\x0D")
    @@character_constants["SO"]        = Lisp::Character.new("\x0E")
    @@character_constants["SI"]        = Lisp::Character.new("\x0F")
    @@character_constants["DLE"]       = Lisp::Character.new("\x10")
    @@character_constants["DC1"]       = Lisp::Character.new("\x11")
    @@character_constants["DC2"]       = Lisp::Character.new("\x12")
    @@character_constants["DC3"]       = Lisp::Character.new("\x13")
    @@character_constants["DC4"]       = Lisp::Character.new("\x14")
    @@character_constants["NAK"]       = Lisp::Character.new("\x15")
    @@character_constants["SYN"]       = Lisp::Character.new("\x16")
    @@character_constants["ETB"]       = Lisp::Character.new("\x17")
    @@character_constants["CAN"]       = Lisp::Character.new("\x18")
    @@character_constants["EM"]        = Lisp::Character.new("\x19")
    @@character_constants["SUB"]       = Lisp::Character.new("\x1A")
    @@character_constants["ESC"]       = Lisp::Character.new("\x1B")
    @@character_constants["FS"]        = Lisp::Character.new("\x1C")
    @@character_constants["GS"]        = Lisp::Character.new("\x1D")
    @@character_constants["RS"]        = Lisp::Character.new("\x1E")
    @@character_constants["US"]        = Lisp::Character.new("\x1F")
    @@character_constants["DEL"]       = Lisp::Character.new("\x7F")


    def self.character_constants()
      @@character_constants
    end

    
    def self.with_value(n)
      if n.length == 1
        ch = Lisp::PrimCharacter.find_character_for_chr(n[0])
        return ch unless ch.nil?
        ch = self.new(n[0])
        @@character_constants[n] = ch
        ch
      elsif @@character_constants.has_key?(n)
        @@character_constants[n]
      elsif n[0..1] == "U+"
        Lisp::PrimCharacter.find_character_for_chr(n[2..-1].to_i(16).chr)
      else
        return Lisp::Debug.process_error("Invalid character name: #{n}", Lisp::EnvironmentFrame.global)
      end
    end

  end

end
