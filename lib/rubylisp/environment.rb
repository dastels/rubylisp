module Lisp

  class Environment < Atom

    def self.with_value(e)
      self.new(e)
    end

    def initialize(e)
      @value = e
    end

    def environment?
      true
    end

    def equal?(other)
      other.environment? && @value == other.value
    end

    def type
      :environment
    end

  end

end
