module Lisp

  class EnvironmentFrame

    attr_accessor :frame, :current_code, :previous, :name
    attr_reader :parent, :project_environment
    
    def self.global
      @@global_frame ||= EnvironmentFrame.new(nil, "GLOBAL")
      @@global_frame
    end

    def self.extending(parent, name, f=nil)
      f ||= parent.frame if parent && parent.has_frame?
      e = self.new(parent, name, f)
      TopLevelEnvironments[name] = e if parent.nil? || parent == self.global
      e
    end

    def initialize(parent, name, f=nil)
      @bindings = []
      @parent = parent
      @name = name
      @frame = f
      @current_code = []
    end


    def clear
      TopLevelEnvironments[@name] = nil if TopLevelEnvironments.has_key?(@name)
      @bindings.each {|b| b.value = nil}
    end
    

    def has_frame?
      !@frame.nil?
    end
    
    # Bindings following parent env frame pointer

    def bound_names
      @bindings.map {|b| b.symbol}
    end

    def bound_values
      @bindings.map {|b| b.value}
    end

    def is_name_bound?(str)
      if !@frame && @frame.has_slot?(Lisp:Symbol.named("#{str}:", true))
        return true
      end
        
      binding = @bindings.detect {|b| b.symbol.name == str}
      return true unless binding.nil?
      return false if @parent.nil?
      return @parent.is_name_bound?(str)
    end

    def name_bound_locally?(str)
      binding = @bindings.detect {|b| b.symbol.name == str}
      !binding.nil?
    end

    def binding_for(symbol)
      binding = @bindings.detect {|b| b.symbol.name == symbol.name}
      return binding unless binding.nil?
      return @parent.binding_for(symbol) unless @parent.nil?
      nil
    end

    def bind(symbol, value)
      b = self.binding_for(symbol)
      if b.nil?
        @bindings << Lisp::Binding.new(symbol, value)
      else
        b.value = value
      end
    end

    def set(symbol, value)
      naked_symbol = symbol.to_naked
      if @frame && @frame.has_slot?(naked_symbol)
        return @frame.at_put(naked_symbol, value)
      end
        
      b = self.binding_for(symbol)
      if b.nil?
        raise "#{symbol} is undefined."
      else
        b.value = value
      end
    end

    # Bindings local to this env frame only

    def local_binding_for(symbol)
      @bindings.detect {|b| b.symbol.name == symbol.name}
    end

    def bind_locally(symbol, value)
      b = self.local_binding_for(symbol)
      if b.nil?
        @bindings << Lisp::Binding.new(symbol, value)
      else
        b.value = value
      end
    end

    # Look up a symbol

    def value_of(symbol)
      b = local_binding_for(symbol)
      return b.value unless b.nil?
                
      naked_symbol = symbol.to_naked
      if @frame && @frame.has_slot?(naked_symbol)
        return @frame.get(naked_symbol)
      end

      b = binding_for(symbol)
      return b.value unless b.nil?
      nil
    end
    

    def quick_value_of(symbol_name)
      b = binding_for(Symbol.new(symbol_name))
      b.nil? ? nil : b.value
    end


    def dump_bindings
      @bindings.each do |b|
        puts b.to_s if b.value.nil? || !b.value.primitive?
      end
      puts
    end


    def dump(frame_number=0)
      puts "Frame #{frame_number}: #{@current_code[0]}"
      dump_bindings
      @previous.dump(frame_number + 1) unless @previous.nil?
    end


    def dump_single_frame(frame_number)
      if frame_number == 0
        puts "Evaling: #{@current_code[0]}"
        dump_bindings
      elsif !@previous.nil?
        @previous.dump_single_frame(frame_number - 1)
      else
        puts "Invalid frame selected."
      end
    end

    def internal_dump_headers(frame_number)
      puts "Frame #{frame_number}: #{@current_code[0]}"
      @previous.internal_dump_headers(frame_number + 1) unless @previous.nil?
    end

    
    
    def dump_headers
      puts
      internal_dump_headers(0)
    end


    def depth
      if @previous.nil?
        1
      else
        1 + @previous.depth
      end
    end

  end

  TopLevelEnvironments = {}

end
