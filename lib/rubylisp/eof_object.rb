module Lisp

  class EofObject < Atom

    def self.instance
      @instance ||= self.new()
    end

    def initialize()
    end

    def type
      :eof_object
    end

    def eof_object?
      true
    end

    def to_s
      "<EOF>"
    end

  end

end
