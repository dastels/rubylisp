module Lisp

  class FfiStatic < FfiSend

    def initialize(name)
      @class_name, @value = name.split('/')
      @klass = NativeObject.with_value(Object.const_get(@class_name))
    end

    def apply_to(args, env)
      a = [@klass] + args.to_a
      super(a, env)
    end

    def to_s
      "#{@class_name}/#{@value}"
    end


  end

end
