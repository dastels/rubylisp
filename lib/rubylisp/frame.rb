module Lisp

  class Frame < Atom

    def self.with_map(m)
      self.new(m)
    end

    def initialize(m = {})
      @value = m
      self
    end


    def clone
      Lisp::Frame.with_map(@value.clone)
    end

    
    def is_parent_key(k)
      k.to_s[-2] == "*"
    end


    def local_slots
      @value.keys
    end


    def inherited_value_slots
      parent_frames = parent_slots.collect {|pk| get(pk)}
      parent_slots = parent_frames.collect {|p| p.inherited_value_slots}
      local_value_slots = Set[local_slots.reject {|s| is_parent_key(k)}]
      parent_slots.inject(local_value_slots) {|all, s| all + s} 
    end
    
    
    def has_parent_slots?
      @value.keys.any? {|k| is_parent_key(k)}
    end


    def parent_slots
      @value.keys.select {|k| is_parent_key(k)}
    end


    def parents
      parent_slots.collect {|pk| @value[pk]}
    end

    
    def has_slot_locally?(n)
      @value.has_key?(n)
    end


    def has_slot_helper(n, v)
      return false if v.include?(self)
      v << self
      return true if has_slot_locally?(n)
      return false unless has_parent_slots?
      return parents.any? {|p| p.has_slot_helper(n, v)}
    end
    
  
    def has_slot?(n)
      has_slot_helper(n, Set.new)
    end
    
  
    def get_helper(key, v)
      return nil if v.include?(self)
      v << self
      return @value[key] if has_slot_locally?(key)
      parents.each do |p|
        value = p.get_helper(key, v)
        return value unless value.nil?
      end
      nil
    end


    def get(key)
      get_helper(key, Set.new)
    end


    def remove(key)
      return false unless has_slot_locally?(key)
      @value.delete(key)
      true
    end


    def at_put(key, value)
      return @value[key] = value
    end

    
    def lisp_object?
      true
    end
    
    def type
      :frame
    end
    
    def empty?
      @value.empty?
    end

    def frame?
      true
    end
    
    def length
      return @value.length
    end

    def car
      nil
    end

    def cdr
      nil
    end

    def equal?(other)
      return false unless other.frame?
      return false unless @value.length == other.value.length
      @value.each do |k, v|
        return false unless other.value[k].equal?(v)
      end
      true
    end

    def to_s
      pairs = @value.collect {|k, v| "#{k.to_s} #{v.to_s}"}
      "{#{pairs.join(' ')}}"
    end
    
  end

end
