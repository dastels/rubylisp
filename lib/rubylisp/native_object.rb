module Lisp

  class NativeObject < Atom
    
    def self.new_instance_of(c)
      self.new(c.alloc.init)
    end

    def self.with_value(o)
      self.new(o)
    end

    def initialize(o=nil)
      @value = o
    end

    def with_value(&block)
      block.call(@value)
    end
    
    def object?
      true
    end

    def type
      :object
    end

    def native_type
      @value.class
    end

    def to_s
      "<a #{@value.class}: #{@value}>"
    end

    def true?
      @value != nil
    end

    def false?
      @value == nil
    end
    
  end

end
