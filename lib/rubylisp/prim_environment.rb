module Lisp

  class PrimEnvironment

    def self.register
      Primitive.register("environment?", "1") {|args, env|  Lisp::PrimEnvironment::environmentp_impl(args, env)}
	  Primitive.register("environment-has-parent?", "1") {|args, env|  Lisp::PrimEnvironment::environment_parentp_impl(args, env) }
	  Primitive.register("environment-bound-names", "1") {|args, env|  Lisp::PrimEnvironment::environment_bound_names_impl(args, env) }
	  Primitive.register("environment-macro-names", "1") {|args, env|  Lisp::PrimEnvironment::environment_macro_names_impl(args, env) }
	  Primitive.register("environment-bindings", "1") {|args, env|  Lisp::PrimEnvironment::environment_bindings_impl(args, env) }
	  Primitive.register("environment-reference-type", "2") {|args, env|  Lisp::PrimEnvironment::environment_reference_type_impl(args, env) }
	  Primitive.register("environment-bound?", "2") {|args, env|  Lisp::PrimEnvironment::environment_boundp_impl(args, env) }
	  Primitive.register("environment-assigned?", "2") {|args, env|  Lisp::PrimEnvironment::environment_assignedp_impl(args, env) }
	  Primitive.register("environment-lookup", "2") {|args, env|  Lisp::PrimEnvironment::environment_lookup_impl(args, env) }
	  Primitive.register("environment-lookup-macro", "2") {|args, env|  Lisp::PrimEnvironment::environment_lookup_macro_impl(args, env) }
	  Primitive.register("environment-assignable?", "2") {|args, env|  Lisp::PrimEnvironment::environment_assignablep_impl(args, env) }
	  Primitive.register("environment-assign!", "3") {|args, env|  Lisp::PrimEnvironment::environment_assign_bang_impl(args, env) }
	  Primitive.register("environment-definable?", "2") {|args, env|  Lisp::PrimEnvironment::environment_definablep_impl(args, env) }
	  Primitive.register("environment-define", "3") {|args, env|  Lisp::PrimEnvironment::environment_define_impl(args, env) }
	  Primitive.register("the-environment", "0") {|args, env|  Lisp::PrimEnvironment::the_environment_impl(args, env) }
	  Primitive.register("procedure-environment", "1") {|args, env|  Lisp::PrimEnvironment::procedure_environment_impl(args, env) }
	  Primitive.register("environment-parent", "1") {|args, env|  Lisp::PrimEnvironment::environment_parent_impl(args, env) }
	  Primitive.register("system-global-environment", "0") {|args, env|  Lisp::PrimEnvironment::system_global_environment_impl(args, env) }
	  Primitive.register("make-top-level-environment", "1|2|3") {|args, env|  Lisp::PrimEnvironment::make_top_level_environment_impl(args, env) }
	  Primitive.register("find-top-level-environment", "1") {|args, env|  Lisp::PrimEnvironment::find_top_level_environment_impl(args, env) }
    end


    def self.environmentp_impl(args, env)
      Lisp::Boolean.with_value(args.car.environment?)
    end

    
	def self.environment_parentp_impl(args, env)
      return Lisp::Debug.process_error("environment-has-parent? requires an environment for it's argument, received: #{args.car}", env) unless args.car.environment?
      Lisp::Boolean.with_value(!args.car.value.parent.nil?)
    end

    
	def self.environment_bound_names_impl(args, env)
      return Lisp::Debug.process_error("environment-bound-names requires an environment for it's argument, received: #{args.car}", env) unless args.car.environment?
      e = args.car.value
      Lisp::ConsCell.array_to_list(e.bound_names)
    end
    
    
	def self.environment_macro_names_impl(args, env)
      return Lisp::Debug.process_error("environment-macro-names requires an environment for it's argument, received: #{args.car}", env) unless args.car.environment?
      e = args.car.value
      Lisp::ConsCell.array_to_list(e.bound_values.select {|v| v.macro?})
    end
    
    
	def self.environment_bindings_impl(args, env)
      return Lisp::Debug.process_error("environment-bindings requires an environment for it's argument, received: #{args.car}", env) unless args.car.environment?
      Lisp::ConsCell.array_to_list(args.car.value.bindings.map { |b| Lisp::ConsCell.array_to_list(b.value.nil? ? [b.symbol] : [b.symbol, b.value]) })
    end
    
    
	def self.environment_reference_type_impl(args, env)
      return Lisp::Debug.process_error("environment-reference-type requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-reference-type requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      b = args.car.value.binding_for(args.cadr.value)
      return Lisp::Symbol.named("unbound") if b.nil?
      return Lisp::Symbol.named("unassigned") if b.value.nil?
      return Lisp::Symbol.named("macro") if b.value.binding?
      Lisp::Symbol.named("normal")
    end

    
	def self.environment_boundp_impl(args, env)
      return Lisp::Debug.process_error("environment-bound? requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-bound? requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      Lisp::Boolean.with_value(args.car.value.name_bound_locally?(args.cadr.name)) 
    end

    
	def self.environment_assignedp_impl(args, env)
      return Lisp::Debug.process_error("environment-assigned? requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-assigned? requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      return Lisp::Debug.process_error("environment-assigned?: #{args.cadr.to_s} is unbound", env) unless args.car.value.name_bound_locally?(args.cadr.name)
      b = args.car.value.local_binding_for(args.cadr)
      return Lisp::Debug.process_error("environment-assigned?: #{args.cadr.to_s} is bound to a macro", env) if b.value.macro?     
      Lisp::Boolean.with_value(!b.value.nil?) 
    end

    
	def self.environment_lookup_impl(args, env)
      return Lisp::Debug.process_error("environment-lookup requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-lookup requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      return Lisp::Debug.process_error("environment-lookup: #{args.cadr.to_s} is unbound", env) unless args.car.value.name_bound_locally?(args.cadr.name)
      b = args.car.value.local_binding_for(args.cadr)
      return Lisp::Debug.process_error("environment-lookup: #{args.cadr.to_s} is unassigned", env) if b.value.nil?
      return Lisp::Debug.process_error("environment-lookup: #{args.cadr.to_s} is bound to a macro", env) if b.value.macro?
      b.value
    end

    
	def self.environment_lookup_macro_impl(args, env)
      return Lisp::Debug.process_error("environment-lookup-macro requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-lookup-macro requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      return Lisp::Debug.process_error("environment-lookup-macro: #{args.cadr.to_s} is unbound", env) unless args.car.value.name_bound_locally?(args.cadr)
      b = args.car.value.local_binding_for(args.cadr)
      return Lisp::Debug.process_error("environment-lookup-macro: #{args.cadr.to_s} is unassigned", env) if b.value.nil?
      return Lisp::Debug.process_error("environment-lookup-macro: #{args.cadr.to_s} is bound to a macro", env) if b.value.macro?
      b.value
    end

    
	def self.environment_assignablep_impl(args, env)
      return Lisp::Debug.process_error("environment-assignable? requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-assignable? requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      local_env = args.car.value
      binding = local_env.binding_for(args.cadr)
      Lisp::Boolean.with_value(!binding.nil?)
    end
    
    
	def self.environment_assign_bang_impl_impl(args, env)
      return Lisp::Debug.process_error("environment-assign! requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-assign! requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      local_env = args.car.value
      binding = local_env.binding_for(args.cadr)
      binding.value = args.caddr unless binding.nil?
    end
    
    
	def self.environment_definablep_impl(args, env)
      return Lisp::Debug.process_error("environment-definable?requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-definable? requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      Lisp::TRUE
    end
    
    
	def self.environment_define_impl(args, env)
      return Lisp::Debug.process_error("environment-define requires an environment for it's first argument, received: #{args.car}", env) unless args.car.environment?
      return Lisp::Debug.process_error("environment-define requires a symbol for it's second argument, received: #{args.cadr}", env) unless args.cadr.symbol?
      args.car.value.bind_locally(args.cadr, args.caddr)
      Lisp::TRUE
    end

    
	def self.the_environment_impl(args, env)
      Lisp::Environment.with_value(env) if env == Lisp.EnvironmentFrame.global || env.parent == Lisp.EnvironmentFrame.global
      Lisp::Debug.process_error("the-environment can only be called from a top-level environment", env)
    end

    
	def self.procedure_environment_impl(args, env)
      Lisp::Debug.process_error("procedure-environment requires a user written function as it's argument", env) unless args.car.function?
      Lisp::Environment.with_value(args.car.env)
    end

    
	def self.environment_parent_impl(args, env)
      return Lisp::Debug.process_error("environment-parent requires an environment for it's argument, received: #{args.car}", env) unless args.car.environment?
      e = args.car.value
      e.parent.nil? ? nil : Lisp::Environment.with_value(e.parent)
    end

    
	def self.system_global_environment_impl(args, env)
      Lisp::Environment.with_value(Lisp::EnvironmentFrame.global)
    end
    
    
	def self.make_top_level_environment_impl(args, env)
      if args.car.string?
        name = args.car.value
        args = args.cdr
      else
        name = "anonymous top level"
      end

      new_env = Lisp::EnvironmentFrame.extending(Lisp::EnvironmentFrame.global, name)
      if args.length == 1
        return Lisp::Debug.process_error("make-top-level-environment expects binding names to be a list", env) unless args.car.list?
        args.to_a.map do |a|
          return Lisp::Debug.process_error("make-top-level-environment expects each binding name to be a symbol", env) unless a.car.symbol?
          new_env.bind_locally_to(a.car, nil)
        end
      elsif args.length == 2
        return Lisp::Debug.process_error("make-top-level-environment expects binding names to be a list", env) unless args.car.list?
        return Lisp::Debug.process_error("make-top-level-environment expects binding values to be a list", env) unless args.cadr.list?
        return Lisp::Debug.process_error("make-top-level-environment expects binding name and value lists to be the same length", env) if args.car.length != args.cadr.length
        args.car.zip(args.cadr).map do |name, value|
          return Lisp::Debug.process_error("make-top-level-environment expects each binding name to be a symbol", env) unless name.symbol?
          new_env.bind_locally_to(name, value)
        end
      end
      return Lisp::Environment.with_value(new_env)
    end

    
	def self.find_top_level_environment_impl(args, env)
      return Lisp::Debug.process_error("find-top-level-environment requires a symbol or sting environment name, received: #{args.cadr}", env) unless args.cadr.symbol? || args.cadr.string
      e = Lisp::TopLevelEnvironments[args.car.to_s]
      return e.nil? ? nil : Lisp::Environment.with_value(e)
    end

  end

end
