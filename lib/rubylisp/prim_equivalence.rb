module Lisp

  class PrimEquivalence

    def self.register
      Primitive.register("=", "2", "(= n1 n2)\n\nEquivalent to (eqv? n1 n2)") do |args, env|
        Lisp::PrimEquivalence::eqv_impl(args, env)
      end
      
      Primitive.register("==", "2", "(== n1 n2)\n\nEquivalent to (eqv? n1 n2)") do |args, env|
        Lisp::PrimEquivalence::eqv_impl(args, env)
      end
      
      Primitive.register("!=", "2", "(!= n1 n2)\n\nEquivalent to (neqv? n1 n2).") do |args, env|
        Lisp::PrimEquivalence::neqv_impl(args, env)
      end
      
      Primitive.register("/=", "2", "(/= n1 n2)\n\nEquivalent to (neqv? n1 n2).") do |args, env|
        Lisp::PrimEquivalence::neqv_impl(args, env)
      end

      Primitive.register("eq?", "2") do |args, env|
        Lisp::PrimEquivalence::eq_impl(args, env)
      end
      
      Primitive.register("neq?", "2") do |args, env|
        Lisp::PrimEquivalence::neq_impl(args, env)
      end
      
      Primitive.register("eqv?", "2") do |args, env|
        Lisp::PrimEquivalence::eqv_impl(args, env)
      end
      
      Primitive.register("neqv?", "2") do |args, env|
        Lisp::PrimEquivalence::eqv_impl(args, env)
      end
      
      Primitive.register("equal?", "2") do |args, env|
        Lisp::PrimEquivalence::equal_impl(args, env)
      end
      
      Primitive.register("nequal?", "2") do |args, env|
        Lisp::PrimEquivalence::nequal_impl(args, env)
      end
      
    end
    

    def self.eqv_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(o1.eqv?(o2))
    end
    

    def self.neqv_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(!o1.eqv?(o2))
    end
    

    def self.eq_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(o1.eq?(o2))
    end
    

    def self.neq_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(!o1.eq?(o2))
    end
    

    def self.equal_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(o1.equal?(o2))
    end
    

    def self.nequal_impl(args, env)
      o1 = args.car
      o2 = args.cadr
      Lisp::Boolean.with_value(!o1.equal?(o2))
    end
    

  end

end
