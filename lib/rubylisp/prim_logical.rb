module Lisp

  class PrimLogical

    def self.register
      Primitive.register("or", ">=2",
                         "(or expression ...\n\nThe expressions are evaluated from left to right, and the value of the first expression that evaluates to a true value is returned. Any remaining expressions are not evaluated. If all expressions evaluate to false values, the value of the last expression is returned. If there are no expressions then #f is returned.",
                         true)  do |args, env|
                           Lisp::PrimLogical::or_impl(args, env)
                         end
      Primitive.register("and", ">=2",
                         "(and expression ...)\n\nThe expressions are evaluated from left to right, and the value of the first expression that evaluates to a false value is returned. Any remaining expressions are not evaluated. If all the expressions evaluate to true values, the value of the last expression is returned. If there are no expressions then #t is returned.",
                         true) do |args, env|
                           Lisp::PrimLogical::and_impl(args, env)
                         end
      Primitive.register("not", "1") {|args, env| Lisp::PrimLogical::not_impl(args, env) }
    end


    def self.or_impl(args, env)
      Lisp::Boolean.with_value(args.inject(false) {|acc, item| acc || item.evaluate(env).value})
    end

    def self.and_impl(args, env)
      Lisp::Boolean.with_value(args.inject(true) {|acc, item| acc && item.evaluate(env).value})
    end

    def self.not_impl(args, env)
      return Lisp::Boolean.with_value(!(args.car.value))
    end

  end
end
