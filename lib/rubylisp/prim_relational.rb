module Lisp

  class PrimRelational

    def self.register
      Primitive.register("<", "2", "(< number number)\n\nReturns whether the first argument is less than the second argument.") do |args, env|
        Lisp::PrimRelational::lt_impl(args, env)
      end

      Primitive.register(">", "2", "(> number number)\n\nReturns whether the first argument is greater than the second argument.") do |args, env|
        Lisp::PrimRelational::gt_impl(args, env)
      end

      Primitive.register("<=", "2", "(<= number number)\n\nReturns whether the first argument is less than or equal to the second argument.") do |args, env|
        Lisp::PrimRelational::lteq_impl(args, env)
      end

      Primitive.register(">=", "2", "(>= number number)\n\nReturns whether the first argument is greater than or equal to the second argument.") do |args, env|
        Lisp::PrimRelational::gteq_impl(args, env)
      end
    end


    def self.lt_impl(args, env)
      return Lisp::Boolean.with_value(args.car.value < args.cadr.value)
    end

    def self.gt_impl(args, env)
      return Lisp::Boolean.with_value(args.car.value > args.cadr.value)
    end

    def self.lteq_impl(args, env)
      return Lisp::Boolean.with_value(args.car.value <= args.cadr.value)
    end

    def self.gteq_impl(args, env)
      return Lisp::Boolean.with_value(args.car.value >= args.cadr.value)
    end


  end
end
