module Lisp

  class Primitive < Atom
    
    attr_reader :doc, :name

    
    def self.register(name, arity, doc="", special=false, env=Lisp::EnvironmentFrame.global, &implementation)
      instance = self.new(name, arity, doc, special, &implementation)
      env.bind(Symbol.named(name), instance)
    end

    
    def initialize(name, arity, doc, special, &implementation)
      @name = name
      @arity = arity
      @doc = doc
      @special = special
      @implementation = implementation
    end

    
    def check_arity(args)
      return true if @arity == "*"

      number_of_args = args.length

      @arity.split("|").map do |term|
        m = /^(\d+)$/.match(term)
        return true if m && number_of_args == m[1].to_i

        m = /^>=(\d+)$/.match(term)
        return true if m && number_of_args >= m[1].to_i
        
        m = /^\((\d+),(\d+)\)$/.match(term)
        return true if m && number_of_args >= m[1].to_i && number_of_args <= m[2].to_i
      end

      false
    end

    
    def apply_to(args, env)
      return Lisp::Debug.process_error("Wrong number of arguments to #{@name}. Expected #{@arity} but got #{args.length}.", env) unless check_arity(args)

      cooked_args = if @special
                      args
                    else
                      Lisp::ConsCell.array_to_list(args.to_a.map {|i| i.evaluate(env)})
                    end
      @implementation.call(cooked_args, env)
    end

    
    def apply_to_without_evaluating(args, env)
      return Lisp::Debug.process_error("Wrong number of arguments to #{@name}. Expected #{@arity} but got #{args.length}.", env) unless check_arity(args)
      @implementation.call(args, env)
    end

    
    def to_s
      if @special
        "<specialform: #{@name}>"
      else
        "<primitive: #{@name}>"
      end
    end

    
    def primitive?
      true
    end

    
    def special?
      @special
    end

    
    def type
      if @special
        :specialform
      else
        :primitive
      end
    end

  end

end
