module Lisp

  class String < Atom
    
    def self.with_value(n)
      self.new(n)
    end

    def initialize(n = '')
      @value = n.to_s
    end

    def set!(n)
      @value = n.to_s
    end

    def string?
      true
    end

    def equal?(other)
      other.string? && self.value == other.value
    end

    def type
      :string
    end

    def to_s
      @value
    end

    def to_sym
      @value.to_sym
    end
    
    def print_string
      "\"#{@value}\""
    end
  end

end
