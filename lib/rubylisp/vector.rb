module Lisp

  class Vector < Atom

    def self.with_array(a)
      self.new(a)
    end

    
    def initialize(a = [])
      @value = a
      self
    end


    def update!(a)
      @value = a
    end

    def type
      :vector
    end


    def vector?
      true
    end



    def empty?
      @value.empty?
    end


    def length
      @value.size
    end


    def add(e)
      @value << e
    end


    def to_a
      @value
    end
    

    def to_s
      "#(#{@value.join(' ')})"
    end


    def at(n)
      @value[n]
    end
    alias_method :nth, :at


    def nth_tail(n)
      return Lisp::Vector.new if n > @value.size
      Lisp::Vector.new(@value[n..-1])
    end
    
    
    def at_put(n, d)
      @value[n] = d
    end


    def set_nth!(n, d)
      at_put(n, d)
    end


    def equal?(other)
      return false unless other.vector?
      return false unless @value.size == other.value.size
      (0..@value.size).each do |i|
        return false unless other.value[i].equal?(value[i])
      end
      true
    end

    def each &block
      @value.each &block
    end

  end

end
