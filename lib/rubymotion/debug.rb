# Copyright 2014 David R. Astels.  All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

  
module Lisp

  class Debug

    class <<self
      attr_accessor :trace, :on_error, :on_entry, :single_step, :interactive, :target_env, :eval_in_debug_repl
    end
      

    def self.register
      self.trace = false
      self.on_error = false
      self.on_entry = []
      self.single_step = false
      self.interactive = false
      self.target_env = nil
      self.eval_in_debug_repl = false
    end


    def self.process_error(error_message, env)
      raise error_message
    end


    # def self.log_eval(sexpr, env)
    # end


    # def self.log_result(result, env)
    # end

    def self.print_dashes(level)
      NSLog("-" * level)
    end

    
    def self.log_eval(sexpr, env)
      if !self.eval_in_debug_repl && self.trace
        depth = env.depth
        NSLog("% #d: " % depth)
        print_dashes(depth)
        NSLog("> #{sexpr.to_s}")
      end
    end


    def self.log_result(result, env)
      if !self.eval_in_debug_repl && self.trace
        depth = env.depth
        NSLog("% #d: <" % depth)
        print_dashes(depth)
        NSLog(" #{result.to_s}")
      end
    end

  
  end
end
