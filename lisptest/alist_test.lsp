;;; -*- mode: Scheme -*-

(context "acons"
         ()
         (assert-eq (acons 'a 1) '((a . 1)))
         (assert-eq (acons 'a 1 (acons 'b 2)) '((a . 1) (b . 2))))

(context "zip"
         ()
         (assert-eq (zip '(a b c) '(1 2 3)) '((c . 3) (b . 2) (a . 1) )))

(context "assoc"
         ()
         (assert-eq (assoc 'a '((a . 1) (b . 2))) '(a . 1))
         (assert-eq (assoc 'b '((a . 1) (b . 2))) '(b . 2)))

(context "rassoc"
         ()
         (assert-eq (rassoc 1 '((a . 1) (b . 2))) '(a . 1))
         (assert-eq (rassoc 2 '((a . 1) (b . 2))) '(b . 2)))

(context "dissoc"
         ()
         (assert-eq (dissoc 'a '((a . 1) (b . 2))) '((b . 2)))
         (assert-eq (dissoc 'b '((a . 1) (b . 2))) '((a . 1))))

