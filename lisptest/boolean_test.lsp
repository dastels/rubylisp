;;; -*- mode: Scheme -*-

(context "not"
         ()
         (assert-true (not #f))
         (assert-false (not #t)))
