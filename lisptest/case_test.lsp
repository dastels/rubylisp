;;; -*- mode: Scheme -*-

(define (test-func x)
    (case x
      ((0) "zero")
      ((1) "one")
      ((2) "two")
      ((3) "three")
      (else "unknown")))

(context "case"
         ()
         (assert-eq (test-func 0) "zero")
         (assert-eq (test-func 1) "one")
         (assert-eq (test-func 2) "two")
         (assert-eq (test-func 3) "three")
         (assert-eq (test-func 5) "unknown"))

;; (define (complex-func x)
;;   (let ((y 1))
;;     (case x
;;       ((0) (set! y 2)
;;          (+ y 1))
;;       ((1) (set! y 5)
;;          (+ y 2))
;;       (else (set! y 10)
;;             (+ y 16)))))

;; (context "complex-case"
;;()
;;           (assert-eq (complex-func 0) 3)
;;           (assert-eq (complex-func 1) 7)
;;           (assert-eq (complex-func 42) 26))
