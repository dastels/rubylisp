;;; -*- mode: Scheme -*-

(context "character-lookup"
         ()
         (assert-eq (char->name #\U+0A) "linefeed")
         (assert-eq (name->char "BEL") #\U+07))

(context "character-comparison"
         ()
         (assert-true (char=? #\BEL #\U+07))
         (assert-false (char=? #\BEL #\U+08))

         (assert-true (char<? #\U+10 #\U+11))
         (assert-false (char<? #\U+11 #\U+10))

         (assert-true (char>? #\U+11 #\U+10))
         (assert-false (char>? #\U+10 #\U+10))

         (assert-true (char<=? #\U+10 #\U+11))
         (assert-true (char<=? #\U+10 #\U+10))
         (assert-false (char<=? #\U+11 #\U+10))

         (assert-true (char>=? #\U+11 #\U+10))
         (assert-true (char>=? #\U+10 #\U+10))
         (assert-false (char>=? #\U+10 #\U+11)))

(context "character-case-independant-comparison"
         ()
         (assert-true (char-ci=? #\a #\a))
         (assert-true (char-ci=? #\a #\A))
         (assert-false (char-ci=? #\a #\b))
         (assert-false (char-ci=? #\a #\B))

         (assert-true (char-ci<? #\A #\B))
         (assert-true (char-ci<? #\a #\B))
         (assert-false (char-ci<? #\b #\a))
         (assert-false (char-ci<? #\b #\A))

         (assert-true (char-ci>? #\B #\A))
         (assert-true (char-ci>? #\B #\a))
         (assert-false (char-ci>? #\a #\b))
         (assert-false (char-ci>? #\A #\b))

         (assert-true (char-ci<=? #\A #\B))
         (assert-true (char-ci<=? #\a #\B))
         (assert-false (char-ci<=? #\b #\a))
         (assert-false (char-ci<=? #\b #\A))
         (assert-true (char-ci<=? #\b #\B))
         (assert-true (char-ci<=? #\b #\b))

         (assert-true (char-ci>=? #\B #\A))
         (assert-true (char-ci>=? #\B #\a))
         (assert-false (char-ci>=? #\a #\b))
         (assert-false (char-ci>=? #\A #\b))
         (assert-true (char-ci>=? #\a #\a))
         (assert-true (char-ci>=? #\A #\a)))

(context "upcase"
         ()
         (assert-eq (char-upcase #\a) #\A)
         (assert-eq (char-upcase #\A) #\A)
         (assert-eq (char-upcase #\BEL) #\BEL))

(context "downcase"
         ()
         (assert-eq (char-downcase #\A) #\a)
         (assert-eq (char-downcase #\a) #\a)
         (assert-eq (char-downcase #\BEL) #\BEL))

(context "char->digit-bad-chars"
         ()
         (assert-false (char->digit #\@))
         (assert-false (char->digit #\BEL)))

(context "char->digit"
         ()
         (assert-eq (char->digit #\0) 0)
         (assert-eq (char->digit #\1) 1)
         (assert-eq (char->digit #\2) 2)
         (assert-eq (char->digit #\3) 3)
         (assert-eq (char->digit #\4) 4)
         (assert-eq (char->digit #\5) 5)
         (assert-eq (char->digit #\6) 6)
         (assert-eq (char->digit #\7) 7)
         (assert-eq (char->digit #\8) 8)
         (assert-eq (char->digit #\9) 9)
         (assert-eq (char->digit #\A) #f))

(context "char->digit-base-10"
         ()
         (assert-eq (char->digit #\0 10) 0)
         (assert-eq (char->digit #\1 10) 1)
         (assert-eq (char->digit #\2 10) 2)
         (assert-eq (char->digit #\3 10) 3)
         (assert-eq (char->digit #\4 10) 4)
         (assert-eq (char->digit #\5 10) 5)
         (assert-eq (char->digit #\6 10) 6)
         (assert-eq (char->digit #\7 10) 7)
         (assert-eq (char->digit #\8 10) 8)
         (assert-eq (char->digit #\9 10) 9)
         (assert-eq (char->digit #\A 10) #f))

(context "char->digit-base-8"
         ()
         (assert-eq (char->digit #\0 8) 0)
         (assert-eq (char->digit #\1 8) 1)
         (assert-eq (char->digit #\2 8) 2)
         (assert-eq (char->digit #\3 8) 3)
         (assert-eq (char->digit #\4 8) 4)
         (assert-eq (char->digit #\5 8) 5)
         (assert-eq (char->digit #\6 8) 6)
         (assert-eq (char->digit #\7 8) 7)
         (assert-eq (char->digit #\8 8) #f))

(context "char->digit-base-16"
         ()
         (assert-eq (char->digit #\0 16) 0)
         (assert-eq (char->digit #\1 16) 1)
         (assert-eq (char->digit #\2 16) 2)
         (assert-eq (char->digit #\3 16) 3)
         (assert-eq (char->digit #\A 16) 10)
         (assert-eq (char->digit #\a 16) 10)
         (assert-eq (char->digit #\f 16) 15)
         (assert-eq (char->digit #\g 16) #f))


(context "digit->char-bad-chars"
         ()
         (assert-false (digit->char 50))
         (assert-false (digit->char -2)))

(context "digit->char"
         ()
         (assert-eq (digit->char 0) #\0)
         (assert-eq (digit->char 1) #\1)
         (assert-eq (digit->char 2) #\2)
         (assert-eq (digit->char 3) #\3)
         (assert-eq (digit->char 4) #\4)
         (assert-eq (digit->char 5) #\5)
         (assert-eq (digit->char 6) #\6)
         (assert-eq (digit->char 7) #\7)
         (assert-eq (digit->char 8) #\8)
         (assert-eq (digit->char 9) #\9))

(context "digit->char-base-10"
         ()
         (assert-eq (digit->char 0 10) #\0)
         (assert-eq (digit->char 1 10) #\1)
         (assert-eq (digit->char 2 10) #\2)
         (assert-eq (digit->char 3 10) #\3)
         (assert-eq (digit->char 4 10) #\4)
         (assert-eq (digit->char 5 10) #\5)
         (assert-eq (digit->char 6 10) #\6)
         (assert-eq (digit->char 7 10) #\7)
         (assert-eq (digit->char 8 10) #\8)
         (assert-eq (digit->char 9 10) #\9))

(context "digit->char-base-8"
         ()
         (assert-eq (digit->char 0 8) #\0)
         (assert-eq (digit->char 1 8) #\1)
         (assert-eq (digit->char 2 8) #\2)
         (assert-eq (digit->char 3 8) #\3)
         (assert-eq (digit->char 4 8) #\4)
         (assert-eq (digit->char 5 8) #\5)
         (assert-eq (digit->char 6 8) #\6)
         (assert-eq (digit->char 7 8) #\7))

(context "digit->char-base-16"
         ()
         (assert-eq (digit->char 0 16) #\0)
         (assert-eq (digit->char 1 16) #\1)
         (assert-eq (digit->char 2 16) #\2)
         (assert-eq (digit->char 3 16) #\3)
         (assert-eq (digit->char 10 16) #\A)
         (assert-eq (digit->char 15 16) #\F))

(context "char->int"
         ()
         (assert-eq (char->integer #\0) 48)
         (assert-eq (char->integer #\[) 91)
         (assert-eq (char->integer #\q) 113))

(context "int->char"
         ()
         (assert-eq (integer->char 48) #\0)
         (assert-eq (integer->char 91) #\[)
         (assert-eq (integer->char 113) #\q))
