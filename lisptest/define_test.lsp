;;; -*- mode: Scheme -*-

(context "defining-a-variable"
         ()
         (assert-eq (begin (define a 1)
                           a)
                    1)
         (assert-eq (begin (define a (+ 1 2))
                           a)
                    3))


(context "defining-a-function-and-calling-with-simple-args"
         ((define (add a b) (+ a b)))
         (assert-eq (add 1 2) 3))

(context "defining-a-function-and-calling-with-complex-args"
         ((define (add a b) (+ a b)))
         (assert-eq (add (* 2 2) (+ 3 4)) 11))

;; (context "functions-referencing-outer-environment"
;;          ((define x 4)
;;           (define (plus-x b) (+ b x)))
;;          (assert-eq (plus-x 2) 6)
;;          (assert-eq (plus-x 10) 14))

(context "closures"
         ((define x 10)
          (define (outer x)
            (define (inner y)
              (+ x y))
            inner)
          (define f1 (outer 1))
          (define f2 (outer 2)))
         (assert-eq (f1 2) 3)
         (assert-eq (f2 2) 4))

(context "lambda"
         ((define f (lambda (x y) (+ x y))))
         (assert-eq (f 1 2) 3)
         (assert-eq ((lambda (x y) (+ x y)) 1 2) 3)
         (assert-eq (apply (lambda (x y) (+ x y)) '(1 2)) 3))



(context "recursive-function"
         ((define (bar x)
            (if (== x 0)
                0
                (+ x (bar (- x 1))))))
         (assert-eq (bar 0) 0)
         (assert-eq (bar 1) 1)
         (assert-eq (bar 2) 3)
         (assert-eq (bar 3) 6)
         (assert-eq (bar 4) 10))

(define (f a b . c)
    (assert-eq a 1)
    (assert-eq b 2)
    (assert-eq c '(3 4 5)))

(context "var-args"
         ()
         (f 1 2 3 4 5))
