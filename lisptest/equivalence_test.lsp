;;; -*- mode: Scheme -*-

(define xx 2)

(define (f) 42)
(define (g) 42)

(define frame1 {a: 1})

(define l1 '(a b c))
(define l1 l2)

(define s1 "hi")
(define s2 s1)

(context "integer-equal-true"
         ()
         (assert-true (== xx xx))
         (assert-true (== 2 xx))
         (assert-true (= xx xx))
         (assert-true (= 2 xx)))

(context "integer-equal-false"
         ()
         (assert-false (== 2 3))
         (assert-true (== 2 2.0))
         (assert-false (= 2 3))
         (assert-true (= 2 2.0)))

(context "integer-not-equal-true"
         ()
         (assert-true (/= 2 3))
         (assert-false (/= 2 2.0))
         (assert-true (!= 2 3))
         (assert-false (!= 2 2.0)))

(context "integer-not-equal-false"
         ()
         (assert-false (/= xx xx))
         (assert-false (/= 2 xx))
         (assert-false (!= xx xx))
         (assert-false (!= 2 xx)))


(context "eqv?-true"
         ()
         (assert-true (eqv? #t #t))
         (assert-true (eqv? #f #f))
         (assert-true (eqv? 'a 'a))
         (assert-true (eqv? 2 2))
         (assert-true (eqv? 2.5 2.5))
         (assert-true (eqv? '() '()))
         (assert-true (eqv? nil nil))
         (assert-true (eqv? f f))
         (assert-true (eqv? l1 l2))
         (assert-true (eqv? s1 s2))
         (assert-true (eqv? frame1 frame1)))

(context "eqv?-false"
         ()
         (assert-false (eqv? #t #f))
         (assert-false (eqv? 'a 'b))
         (assert-false (eqv? 2 3))
         (assert-false (eqv? 2.5 3.7))
         (assert-true (eqv? 2 2.0))
         (assert-false (eqv? '() '(1)))
         (assert-false (eqv? f g))
         (assert-false (eqv? l1 '(a b c)))
         (assert-false (eqv? "hi" "hi"))
         (assert-false (eqv? {a: 1} {a: 1})))

(context "equal?-true"
         ()
         (assert-true (equal? 'a 'a))
         (assert-true (equal? '(a) '(a)))
         (assert-true (equal? '(a (b) c)
                              '(a (b) c)))
         (assert-true (equal? "abc" "abc"))
         (assert-true (equal? 2 2))
         (assert-true (equal? {a: 1} {a: 1})))
