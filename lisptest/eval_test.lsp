;;; -*- mode: Scheme -*-

(context "eval"
         ()
         (assert-eq (eval '(+ 1 2 3)) 6))
