;;; -*- mode: Scheme -*-


(context "frame-rendering"
         ()
         (assert-eq (str (make-frame a: 1)) "{a: 1}"))

(context "naked-symbols"
         ()
         (assert-eq a: 'a:))

(context "frame-access"
         ()
         (assert-eq (get-slot {a: 1 b: 2 c: 3} a:) 1)
         (assert-eq (get-slot {a: 1 b: 2 c: 3} b:) 2))

(context "frame-mutation"
         ()
         (let ((f {a: 1 b: 2 c: 3}))
           (assert-eq (set-slot! f a: 5) 5)
           (assert-eq (get-slot f a:) 5)))

(context "frame-method"
         ()
         (let ((f {a: 5
                  b: 2
                  foo: (lambda (x)
                         (+ x a))}))
           (assert-eq (send f foo: 1) 6))
         (let ((f {a: 5
                  b: 2
                  foo: (lambda (x)
                         (set! b (+ x a)))}))
           (assert-eq (send f foo: 1) 6)
           (assert-eq (get-slot f b:) 6)))

(context "prototypes"
         ()
         (let* ((f {a: 2
                   b: 1})
                (g {parent*: f
                   a: 3}))
           (assert-eq (get-slot g b:) 1)
           (assert-eq (get-slot g a:) 3)
           (set-slot! g a: 1)
           (assert-eq (get-slot g a:) 1)
           (set-slot! g b: 10)
           (assert-eq (get-slot g b:) 10))
         (let* ((adder {add: (lambda (x)
                               (+ x a))})
                (incrementor {parent*: adder
                             a: 1}))
           (assert-eq (send incrementor add: 3) 4)))

(context "new-slots"
         ()
         (let ((f {a: 1}))
           (assert-eq (set-slot! f b: 5) 5)
           (assert-eq (get-slot f b:) 5)))

(context "function-slot-use"
         ()
         (let ((f {a: 5
                  b: 2
                  foo: (lambda (x)
                         (+ x a))
                  bar: (lambda ()
                         (foo b))}))
           (assert-eq (send f bar:) 7)))

(context "inherited-function-slot-use"
         ()
         (let* ((f {a: 5
                   foo: (lambda (x)
                          (+ x a))})
                (g {parent*: f
                   b: 2
                   bar: (lambda ()
                          (foo b))}))
           (assert-eq (send g bar:) 7)))

(context "multiple-parents"
         ()
         (let* ((e {a: 5})
                (f {b: 2})
                (g {parent-e*: e
                   parent-f*: f
                   foo: (lambda (x)
                          (+ x a))
                   bar: (lambda ()
                          (foo b))}))
           (assert-eq (send g bar:) 7)
           (set-slot! g a: 10)
           (assert-eq (get-slot g a:) 10)
           (assert-eq (get-slot e a:) 5)))

(context "calling-super"
         ()
         (let* ((f {foo: (lambda () 42)})
                (g {parent*: f  foo: (lambda () (+ 1 (send-super foo:)))}))
           (assert-eq (send g foo:) 43)))

(context "locals-override-slots"
         ()
         (let* ((f {a: 42})
                (g {parent*: f  foo: (lambda ()
                                       (let ((a 10))
                                         (+ 1 a)))}))
           (assert-eq (send g foo:) 11)))

(context "cloning"
         ()
         (let* ((f {a: 1 b: 2})
                (g (clone f)))
           (assert-eq (equal? f g) #t)
           (set-slot! f a: 42)
           (assert-eq (get-slot f a:) 42)
           (assert-eq (get-slot g a:) 1)))

(context "has-slot"
         ()
         (let ((f {a: 1 b: 2}))
           (assert-true (has-slot? f a:))
           (assert-true (has-slot? f b:))
           (assert-false (has-slot? f c:))))

(context "remove-slots"
         ()
         (let* ((e {a: 5})
                (f {b: 2})
                (g {parent-e*: e
                   parent-f*: f
                   foo: 3
                   bar: 10}))
           (assert-true (remove-slot! g foo:))
           (assert-false (has-slot? g foo:))
           (assert-false (remove-slot! g a:))
           (assert-true (has-slot? e a:))))

(context "keys"
         ()
         (let ((f {a: 1 b: 2}))
           (assert-eq (frame-keys f) '(a: b:))))

(context "shortcuts"
         ()
         (let ((f {a: 1 b: 2}))
           (assert-true (a:? f))
           (assert-false (c:? f))
           (assert-eq (a: f) 1)
           (assert-eq (b: f) 2)
           (a:! f 42)
           (assert-eq (a: f) 42)))
