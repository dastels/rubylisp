;;; -*- mode: Scheme -*-

(context "if-with-false-condition"
         ()
         (assert-eq (if #f 1) nil)
         (assert-eq (if #f 1 2) 2))

(context "if-with-true-condition"
         ()
         (assert-eq (if #t 1) 1)
         (assert-eq (if #t 1 2) 1))
