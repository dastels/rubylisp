;;; -*- mode: Scheme -*-

(context "simple-let"
         ()
         (assert-eq (let ())
                    nil)
         (assert-eq (let ()
                      42)
                    42))

(context "let-with-multiple-expr-body"
         ()
         (assert-eq (let ()
                      1
                      2)
                    2))

(context "let-bindings"
         ()
         (assert-eq (let ((x 1)
                          (y 2))
                      (+ x y))
                    3)
         (assert-eq (let* ((x 1)
                           (y (+ x 1)))
                      y)
                    2))


(context "let-binding-scope"
         ()
         (assert-eq (begin (let ((zz 2)) zz)
                           zz)
                    nil))

(context "let-let*"
         ((define x 10))
         (assert-eq (let ((x 5)
                          (y (* x 2)))
                      (+ x y))
                    25)
         (assert-eq (let* ((x 5)
                           (y (* x 2)))
                      (+ x y))
                    15))

(context "named let"
         ()
         (assert-eq (let foo ((a 0) (b 10))
                     (if (zero? b)
                         a
                         (foo (+ a 1) (- b 1))))
                    10)
         (assert-eq (let foo ((a 0) (b 10))
                      (if (zero? b)
                          a
                          (foo (+ a 2) (- b 1))))
                    20))
