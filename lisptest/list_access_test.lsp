;;; -*- mode: Scheme -*-

                                        ; Test list access primitives

(define l '(1 2 3 4 5 6 7 8 9 10))

(context "list"
         ()
         (assert-eq (list 'a) '(a))
         (assert-eq (list (+ 1 1) (+ 1 2)) '(2 3)))

(context "length"
         ()
         (assert-eq (length nil) 0)
         (assert-eq (length '()) 0)
         (assert-eq (length '(1)) 1)
         (assert-eq (length '(1 2)) 2)
         (assert-eq (length l) 10))

(context "first"
         ()
         (assert-eq (first '(1 2 3 4 5 6 7 8 9 10)) 1)
         (assert-eq (first l) 1))

(context "second"
         ()
         (assert-eq (second '(1 2 3 4 5 6 7 8 9 10)) 2)
         (assert-eq (second l) 2))

(context "third"
         ()
         (assert-eq (third '(1 2 3 4 5 6 7 8 9 10)) 3)
         (assert-eq (third l) 3))

(context "fourth"
         ()
         (assert-eq (fourth '(1 2 3 4 5 6 7 8 9 10)) 4)
         (assert-eq (fourth l) 4))

(context "fifth"
         ()
         (assert-eq (fifth '(1 2 3 4 5 6 7 8 9 10)) 5)
         (assert-eq (fifth l) 5))

(context "sixth"
         ()
         (assert-eq (sixth '(1 2 3 4 5 6 7 8 9 10)) 6)
         (assert-eq (sixth l) 6))

(context "seventh"
         ()
         (assert-eq (seventh '(1 2 3 4 5 6 7 8 9 10)) 7)
         (assert-eq (seventh l) 7))

(context "eighth"
         ()
         (assert-eq (eighth '(1 2 3 4 5 6 7 8 9 10)) 8)
         (assert-eq (eighth l) 8))

(context "ninth"
         ()
         (assert-eq (ninth '(1 2 3 4 5 6 7 8 9 10)) 9)
         (assert-eq (ninth l) 9))

(context "tenth"
         ()
         (assert-eq (tenth '(1 2 3 4 5 6 7 8 9 10)) 10)
         (assert-eq (tenth l) 10))

(context "nth"
         ()
         (assert-eq (nth 0 l) 1)
         (assert-eq (nth 1 l) 2)
         (assert-eq (nth 2 l) 3)
         (assert-eq (nth 3 l) 4)
         (assert-eq (nth 4 l) 5)
         (assert-eq (nth 5 l) 6)
         (assert-eq (nth 6 l) 7)
         (assert-eq (nth 7 l) 8)
         (assert-eq (nth 8 l) 9)
         (assert-eq (nth 9 l) 10))

(context "car"
         ()
         (assert-eq (car nil) nil)
         (assert-eq (car '(1)) 1)
         (assert-eq (car l) 1))

(context "cdr"
         ()
         (assert-eq (cdr nil) nil)
         (assert-eq (cdr '(1)) nil)
         (assert-eq (length (cdr l)) 9))

(context "rest"
         ()
         (assert-eq (rest nil) nil)
         (assert-eq (rest '(1)) nil)
         (assert-eq (rest '(1 2 3)) '(2 3)))

(context "caar"
         ()
         (assert-eq (caar nil) nil)
         (assert-eq (caar '((1))) 1))

(context "cadr"
         ()
         (assert-eq (cadr nil) nil)
         (assert-eq (cadr '(1 2)) 2))

(context "cdar"
         ()
         (assert-eq (cdar nil) nil)
         (assert-eq (cdar '(1)) nil)
         (assert-eq (cdar '((1 2) 3)) '(2)))

(context "cddr"
         ()
         (assert-eq (cddr nil) nil)
         (assert-eq (cddr '(1)) nil)
         (assert-eq (cddr '(1 2 3)) '(3)))

(context "sublist"
         ()
         (assert-eq (sublist '(a b c d e f) 2 5) '(c d e)))

(context "list-head"
         ()
         (assert-eq (list-head '(a b c d e f) 3) '(a b c)))

(context "take"
         ()
         (assert-eq (take 3 '(a b c d e f)) '(a b c)))

(context "list-tail"
         ()
         (assert-eq (list-tail '(a b c d e f) 3) '(d e f)))

(context "drop"
         ()
         (assert-eq (drop 3 '(a b c d e f)) '(d e f)))

(context "last-pair"
         ()
         (assert-eq (last-pair '(a b c d)) '(d))
         (assert-eq (last-pair '(a b c . d)) '(c . d)))

