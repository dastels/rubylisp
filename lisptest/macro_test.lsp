;;; -*- mode: Scheme -*-

(context "quasiquoted-literal"
         ()
         (assert-eq `a 'a)
         (assert-eq `1 1))

(context "quasiquoted-list"
         ()
         (assert-eq `(a b c) '(a b c))
         (assert-eq `(1 (2) 3) '(1 (2) 3)))

(context "unquote"
         ()
         (assert-eq `(a ,(+ 1 2) b) '(a 3 b)))

(context "unquote-splicing"
         ()
         (assert-eq `(a ,@(list 1 2 3) b) '(a 1 2 3 b)))

(context "combined-and-eval"
         ()
         (let ((x 1)
               (y '(2 3)))
           (assert-eq (eval `(+ ,x ,@y)) 6)))

(context "defmacro"
         ((defmacro (add x y)
            `(+ ,x ,@y)))
         (assert-eq (add 1 (2 3)) 6))

(context "defmacro-with-varargs"
         ((defmacro (add x . y)
            `(+ ,x ,@y)))
         (assert-eq (add 1 2 3) 6))

(context "expand"
         ((defmacro (add x . y)
            `(+ ,x ,@y)))
         (assert-eq (expand add 1 2 3) '(+ 1 2 3)))

(context "nested"
         ()
         (assert-eq  `(a `(b ,(+ 1 2) ,(foo ,(+ 1 3) d) e) f) '(a `(b ,(+ 1 2) ,(foo 4 d) e) f)))
