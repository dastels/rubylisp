;;; -*- mode: Scheme -*-

(define xx 2)
(define y 8)
(define z 7)

(context "arithmetic-test"
         ()
         (assert-eq (+ 5 5) 10)
         (assert-eq (- 10 7) 3)
         (assert-eq (* 2 4) 8)
         (assert-eq (/ 25 5) 5)
         (assert-eq (% 7 5) 2)
         (assert-eq (modulo 7 5) 2)
         (assert-eq (truncate 5) 5)
         (assert-eq (truncate 5.43) 5))

(context "rounding"
         ()
         (assert-eq (round PI) 3)
         (assert-eq (round 3.5) 4)
         (assert-eq (round 2.5) 2)
         (assert-eq (round -2.5) -2)
         (assert-eq (round -3.4) -3)
         (assert-eq (round -3.5) -4)
         (assert-eq (truncate PI) 3)
         (assert-eq (truncate 3.5) 3)
         (assert-eq (truncate -3.5) -3)
         (assert-eq (ceiling PI) 4)
         (assert-eq (ceiling 3.5) 4)
         (assert-eq (ceiling -3.5) -3)
         (assert-eq (floor PI) 3)
         (assert-eq (floor 3.5) 3)
         (assert-eq (floor -3.5) -4))

(context "number-tests"
         ()
         (assert-true (even? 0))
         (assert-true (even? 2))
         (assert-true (even? -2))
         (assert-false (even? 3))
         (assert-false (even? -3))

         (assert-false (odd? 0))
         (assert-false (odd? 2))
         (assert-false (odd? -2))
         (assert-true (odd? 3))
         (assert-true (odd? -3))

         (assert-true (negative? -1))
         (assert-false (negative? 0))
         (assert-false (negative? 1))

         (assert-false (zero? -1))
         (assert-true (zero? 0))
         (assert-false (zero? 1))

         (assert-false (positive? -1))
         (assert-false (positive? 0))
         (assert-true (positive? 1))

         (assert-true (integer? 1))
         (assert-false (integer? 1.0))

         (assert-true (float? 1.0))
         (assert-false (float? 1)))

(context "interval"
         ()
         (assert-eq (interval 1 1) '(1))
         (assert-eq (interval 1 2) '(1 2))
         (assert-eq (interval 1 5) '(1 2 3 4 5)))

(context "relational-test"
         ()
         (assert-eq (< xx y) #t)
         (assert-eq (< y z) #f)
         (assert-eq (> xx y) #f)
         (assert-eq (> z xx) #t)
         (assert-eq (<= xx 2) #t)
         (assert-eq (>= z 7) #t)
         (assert-eq (not #f) #t)
         (assert-eq (not #t) #f)
         (assert-eq (or #f #f) #f)
         (assert-eq (or #f #f #t) #t)
         (assert-eq (and #t #f #t) #f)
         (assert-eq (and #t #t #t) #t)
         (assert-eq (or (> xx z) (> y z)) #t)
         (assert-eq (and (> xx z) (>y z)) #f))

(context "abs"
         ()
         (assert-eq (abs -1) 1)
         (assert-eq (abs 0) 0)
         (assert-eq (abs -1) 1))

(context "trig"
         ()
         (assert-eq (sin PI) 0.0)
         (assert-eq (cos PI) -1.0)
         (assert-eq (tan PI) (/ (sin PI) (cos PI)))
         (assert-eq (sqrt (ceiling (abs (cos PI)))) 1.0))

(context "min"
         ()
         (assert-eq (min 3 2) 2)
         (assert-eq (min 5 -2) -2)
         (assert-eq (min -3 -10) -10)
         (assert-eq (min 3 7 1 6) 1))

(context "max"
         ()
         (assert-eq (max 3 2) 3)
         (assert-eq (max 5 -2) 5)
         (assert-eq (max -3 -10) -3)
         (assert-eq (max 3 7 1 6) 7))
