;;; -*- mode: Scheme -*-

(define x 4)

(context "set-in-global-context"
         ()
         (assert-eq x 4)
         (assert-eq (begin (set! x 10)
                           x)
                    10)
         (assert-eq x 10))

(define y 5)

(context "set-in-local-context"
         ()
         (assert-eq y 5)
         (assert-eq (let ((y 2))
                      (set! y 15)
                      y)
                    15)
         (assert-eq y 5))

(context "set-car"
         ()
         (assert-eq (let ((pair '(a b)))
                      (set-car! pair 1)
                      (car pair))
                    1))

(context "set-cdr"
         ()
         (assert-eq (let ((pair '(a b)))
                      (set-cdr! pair 1)
                      (cdr pair))
                    1))

(context "set-nth"
         ()
         (assert-eq (set-nth! 1 '(1 2 3) 42) '(1 42 3)))


