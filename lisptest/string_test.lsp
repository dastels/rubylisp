;;; -*- mode: Scheme -*-

(context "string?"
         ()
         (assert-true (string? "bar"))
         (assert-false (string? 3)))


(context "str"
         ()
         (assert-eq (str "hello") "hello")
         (assert-eq (str 1) "1")
         (assert-eq (str #t) "#t")
         (assert-eq (str 'a) "a")
         (assert-eq (str '(1 2 3)) "(1 2 3)")
         (assert-eq (str 1 "hello" "-" '(1 2 3)) "1hello-(1 2 3)"))

(context "make-string"
         ()
         (assert-eq (make-string 5 #\a) "aaaaa")
         (assert-eq (make-string 5) "     "))

(context "string"
         ()
         (assert-eq (string) "")
         (assert-eq (string #\a) "a")
         (assert-eq (string #\a #\space #\b) "a b"))

(context "list->string"
         ()
         (assert-eq (list->string '()) "")
         (assert-eq (list->string '(#\a)) "a")
         (assert-eq (list->string '(#\a #\space #\b)) "a b"))

(context "string->list"
         ()
         (assert-eq (string->list "") '())
         (assert-eq (string->list "a") '(#\a))
         (assert-eq (string->list "a b") '(#\a #\space #\b)))

(context "string-copy"
         ()
         (assert-eq (string-copy "abc") "abc"))

(context "string-length"
         ()
         (assert-eq (string-length "") 0)
         (assert-eq (string-length "a") 1)
         (assert-eq (string-length "abcd") 4))

(context "string-null?"
         ()
         (assert-true (string-null? ""))
         (assert-false (string-null? "a"))
         (assert-false (string-null? "abcd")))

(context "string-ref"
         ()
         (assert-eq (string-ref "Hello" 0) #\H)
         (assert-eq (string-ref "Hello" 1) #\e)
         (assert-eq (string-ref "Hello" 2) #\l)
         (assert-eq (string-ref "Hello" 3) #\l)
         (assert-eq (string-ref "Hello" 4) #\o)
         (assert-eq (string-ref "Hello" 5) #f))

(context "string-set!"
         ((define s "Dog"))
         (string-set! s 0 #\L)
         (assert-eq s "Log")
         (assert-eq (string-set! s 3 #\t) #f))

(context "string=?"
         ()
         (assert-true (string=? "PIE" "PIE"))
         (assert-false (string=? "PIE" "pie")))

(context "string-ci=?"
         ()
         (assert-true (string-ci=? "PIE" "PIE"))
         (assert-true (string-ci=? "PIE" "pie")))

(context "substring=?"
         ()
         (assert-true (substring=? "Alamo" 1 3 "cola" 2 4))
         (assert-false (substring=? "Alamo" 1 3 "COLA" 2 4))
         (assert-false (substring=? "Alamo" 1 3 "colt" 2 4)))

(context "substring-ci=?"
         ()
         (assert-true (substring-ci=? "Alamo" 1 3 "cola" 2 4))
         (assert-true (substring-ci=? "Alamo" 1 3 "COLA" 2 4))
         (assert-false (substring-ci=? "Alamo" 1 3 "colt" 2 4))
         (assert-false (substring-ci=? "Alamo" 1 3 "COLT" 2 4)))

(context "string<?"
         ()
         (assert-true (string<? "cat" "dog"))
         (assert-true (string<? "cat" "catkin"))
         (assert-false (string<? "cat" "DOG"))
         (assert-false (string<? "cat" "cat")))

(context "string-ci<?"
         ()
         (assert-true (string-ci<? "cat" "dog"))
         (assert-true (string-ci<? "cat" "catkin"))
         (assert-true (string-ci<? "cat" "DOG"))
         (assert-false (string-ci<? "cat" "cat")))

(context "substring<?"
         ()
         (assert-true (substring<? "-cat-" 1 4 "-cat-" 1 5))
         (assert-true (substring<? "-cat-" 1 4 "-dog-" 1 4))
         (assert-false (substring<? "-cat-" 1 4 "-DOG-" 1 4))
         (assert-false (substring<? "-cat-" 1 4 "-cat-" 1 4)))

(context "substring-ci<?"
         ()
         (assert-true (substring-ci<? "-cat-" 1 4 "-cat-" 1 5))
         (assert-true (substring-ci<? "-cat-" 1 4 "-dog-" 1 4))
         (assert-true (substring-ci<? "-cat-" 1 4 "-DOG-" 1 4))
         (assert-false (substring-ci<? "-cat-" 1 4 "-cat-" 1 4)))

(context "string<=?"
         ()
         (assert-true (string<=? "cat" "dog"))
         (assert-true (string<=? "cat" "catkin"))
         (assert-false (string<=? "cat" "DOG"))
         (assert-true (string<=? "cat" "cat")))

(context "string-ci<=?"
         ()
         (assert-true (string-ci<=? "cat" "dog"))
         (assert-true (string-ci<=? "cat" "catkin"))
         (assert-true (string-ci<=? "cat" "DOG"))
         (assert-true (string-ci<=? "cat" "cat")))

(context "substring<=?"
         ()
         (assert-true (substring<=? "-cat-" 1 4 "-cat-" 1 5))
         (assert-true (substring<=? "-cat-" 1 4 "-dog-" 1 4))
         (assert-false (substring<=? "-cat-" 1 4 "-DOG-" 1 4))
         (assert-true (substring<=? "-cat-" 1 4 "-cat-" 1 4)))

(context "substring-ci<=?"
         ()
         (assert-true (substring-ci<=? "-cat-" 1 4 "-cat-" 1 5))
         (assert-true (substring-ci<=? "-cat-" 1 4 "-dog-" 1 4))
         (assert-true (substring-ci<=? "-cat-" 1 4 "-DOG-" 1 4))
         (assert-true (substring-ci<=? "-CAT-" 1 4 "-cat-" 1 4)))

(context "string>=?"
         ()
         (assert-true (string>=? "dog" "cat"))
         (assert-true (string>=? "catkin" "cat"))
         (assert-false (string>=? "DOG" "cat"))
         (assert-true (string>=? "cat" "cat")))

(context "string-ci>=?"
         ()
         (assert-true (string-ci>=? "dog" "cat"))
         (assert-true (string-ci>=? "catkin" "cat"))
         (assert-true (string-ci>=? "DOG" "cat"))
         (assert-true (string-ci>=? "cat" "cat")))

(context "substring>=?"
         ()
         (assert-true (substring>=? "-cat-" 1 5 "-cat-" 1 4))
         (assert-true (substring>=? "-dog-" 1 4 "-cat-" 1 4))
         (assert-false (substring>=? "-DOG-" 1 4 "-cat-" 1 4))
         (assert-true (substring>=? "-cat-" 1 4 "-cat-" 1 4)))

(context "substring-ci>=?"
         ()
         (assert-true (substring-ci>=? "-cat-" 1 5 "-cat-" 1 4))
         (assert-true (substring-ci>=? "-dog-" 1 4 "-cat-" 1 4))
         (assert-true (substring-ci>=? "-DOG-" 1 4 "-cat-" 1 4))
         (assert-true (substring-ci>=? "-cat-" 1 4 "-CAT-" 1 4)))

(context "string-compare"
         ((define (f-eq) "" "eq")
          (define (f-lt) "" "lt")
          (define (f-gt) "" "gt"))
         (assert-eq (string-compare "a" "a" f-eq f-lt f-gt) "eq")
         (assert-eq (string-compare "a" "b" f-eq f-lt f-gt) "lt")
         (assert-eq (string-compare "b" "a" f-eq f-lt f-gt) "gt")
         (assert-eq (string-compare "a" "a" (lambda () (+ 1 1)) f-lt f-gt) 2)
         (assert-eq (string-compare "a" "b" f-eq (lambda () (+ 1 1)) f-gt) 2)
         (assert-eq (string-compare "b" "a" f-eq f-lt (lambda () (+ 1 1))) 2))

(context "string-compare-ci"
         ((define (f-eq) "" "eq")
          (define (f-lt) "" "lt")
          (define (f-gt) "" "gt"))
         (assert-eq (string-compare-ci "a" "A" f-eq f-lt f-gt) "eq")
         (assert-eq (string-compare-ci "a" "B" f-eq f-lt f-gt) "lt")
         (assert-eq (string-compare-ci "b" "A" f-eq f-lt f-gt) "gt")
         (assert-eq (string-compare-ci "a" "A" (lambda () (+ 1 1)) f-lt f-gt) 2)
         (assert-eq (string-compare-ci "a" "B" f-eq (lambda () (+ 1 1)) f-gt) 2)
         (assert-eq (string-compare-ci "b" "A" f-eq f-lt (lambda () (+ 1 1))) 2))

(context "string-hash"
         ()
         (assert-eq (string-hash "hashstring") (string-hash "hashstring"))
         (assert-neq (string-hash "hashstring") (string-hash "hashstrinh")))

(context "string-hash-mod"
         ()
         (assert-eq (string-hash-mod "hashstring" 15) (modulo (string-hash "hashstring") 15)))

(context "string-capitalized?"
         ()
         (assert-true (string-capitalized? "Hi"))
         (assert-true (string-capitalized? "Hi there"))
         (assert-true (string-capitalized? "Hi There"))
         (assert-true (string-capitalized? "Hi 7465  There"))
         (assert-false (string-capitalized? "hi")))

(context "substring-capitalized?"
         ()
         (assert-true (substring-capitalized? "Hi" 0 2))
         (assert-true (substring-capitalized? "Hi there you" 0 8))
         (assert-true (substring-capitalized? "Hi There you" 3 12))
         (assert-false (substring-capitalized? "Hi there you" 3 12)))

(context "string-lower-case?"
         ()
         (assert-true (string-lower-case? "hello"))
         (assert-true (string-lower-case? "hello, there 123"))
         (assert-false (string-lower-case? "Hello")))

(context "substring-lower-case?"
         ()
         (assert-false (substring-lower-case? "Hi" 0 2))
         (assert-false (substring-lower-case? "Hi there you" 0 8))
         (assert-true (substring-lower-case? "Hi there you" 3 12)))

(context "string-upper-case?"
         ()
         (assert-false (string-upper-case? "hello"))
         (assert-true (string-upper-case? "HELLO, THERE 123"))
         (assert-false (string-upper-case? "Hello")))

(context "substring-upper-case?"
         ()
         (assert-true (substring-upper-case? "HI" 0 2))
         (assert-false (substring-upper-case? "Hi" 0 2))
         (assert-true (substring-upper-case? "HI THERE you" 0 8))
         (assert-false (substring-upper-case? "Hi there you" 0 8)))

(context "string-capitalize"
         ()
         (assert-eq (string-capitalize "hello") "Hello")
         (assert-eq (string-capitalize "Hello") "Hello")
         (assert-eq (string-capitalize "HELLO") "Hello")
         (assert-eq (string-capitalize "heLLo") "Hello"))

(context "string-capitalize!"
         ((define a "heLLo"))
         (string-capitalize! a)
         (assert-eq a "Hello"))

(context "substring-capitalize!"
         ((define a "heLLo"))
         (substring-capitalize! a 2 4)
         (assert-eq a "heLlo"))

(context "string-downcase"
         ()
         (assert-eq (string-downcase "hello") "hello")
         (assert-eq (string-downcase "Hello") "hello")
         (assert-eq (string-downcase "HELLO") "hello")
         (assert-eq (string-downcase "heLLo") "hello"))

(context "string-downcase!"
         ((define a "heLLo"))
         (string-downcase! a)
         (assert-eq a "hello"))

(context "substring-downcase!"
         ((define a "HeLLO"))
         (substring-downcase! a 2 4)
         (assert-eq a "HellO"))

(context "string-upcase"
         ()
         (assert-eq (string-upcase "hello") "HELLO")
         (assert-eq (string-upcase "Hello") "HELLO")
         (assert-eq (string-upcase "HELLO") "HELLO")
         (assert-eq (string-upcase "heLLo") "HELLO"))

(context "string-upcase!"
         ((define a "heLLo"))
         (string-upcase! a)
         (assert-eq a "HELLO"))

(context "substring-upcase!"
         ((define a "hello"))
         (substring-upcase! a 2 4)
         (assert-eq a "heLLo"))

(context "string-append"
         ((define sss "hello"))
         (assert-eq (string-append) "")
         (assert-eq (string-append "*" "ace" "*") "*ace*")
         (assert-eq (string-append "" "" "") "")
         (assert-eq sss (string-append sss)))

(context "substring"
         ()
         (assert-eq (substring "" 0 0) "")
         (assert-eq (substring "arduous" 2 5) "duo"))

(context "string-head"
         ()
         (assert-eq (string-head "hello" 3) "hel")
         (assert-eq (string-head "hello" 0) ""))

(context "string-head"
         ()
         (assert-eq (string-tail "hello" 3) "lo")
         (assert-eq (string-tail "hello" 0) "hello"))

(context "string-pad-left"
         ()
         (assert-eq (string-pad-left "hello" 4) "ello")
         (assert-eq (string-pad-left "hello" 8) "   hello")
         (assert-eq (string-pad-left "hello" 8 #\*) "***hello"))

(context "string-pad-right"
         ()
         (assert-eq (string-pad-right "hello" 4) "hell")
         (assert-eq (string-pad-right "hello" 8) "hello   ")
         (assert-eq (string-pad-right "hello" 8 #\*) "hello***"))

(context "string-trim"
         ()
         (assert-eq (string-trim " in the end ") "in the end")
         (assert-eq (string-trim " ") "")
         (assert-eq (string-trim "100th" "[[:digit:]]") "100"))

(context "string-trim-left"
         ()
         (assert-eq (string-trim-left "-.-+-=-" "\+") "+-=-")
         (assert-eq (string-trim-left "   hello") "hello"))

(context "string-trim-right"
         ()
         (assert-eq (string-trim-right "-.-+-=-" "\+") "-.-+")
         (assert-eq (string-trim-right "hello   ") "hello"))
