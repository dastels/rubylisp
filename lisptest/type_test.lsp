;;; -*- mode: Scheme -*-


(context "type-test"
         ((define a 4)
          (define foo (lambda (x) (+ x x))))
         (assert-true (list? '(list 1 2 3)))
         (assert-false (list? a))
         (assert-false (pair? #f))
         (assert-true (nil? '()))
         (assert-false (nil? a))
         (assert-true (not-nil? a))
         (assert-false (not-nil? '()))
         (assert-false (symbol? a))
         (assert-true (symbol? 'a))
         (assert-false (symbol? "bar"))
         (assert-true (number? a))
         (assert-false (number? "bar"))
         (assert-true (function? foo))
         (assert-false (function? 1))
         (assert-true (char? #\a))
         (assert-false (char? 1)))
