Gem::Specification.new do |gem|
  gem.name        = 'rubymotionlisp'
  gem.version     = '1.0.11'
  gem.date        = '2016-01-20'
  gem.summary     = "Lisp in RubyMotion"
  gem.description = "An embeddable Lisp as an extension language for RubyMotion"
  gem.authors     = ["Dave Astels"]
  gem.email       = 'dastels@icloud.com'
  gem.files       = ["lib/rubymotionlisp.rb",
                     "lib/rubylisp/atom.rb",
                     "lib/rubylisp/binding.rb",
                     "lib/rubylisp/boolean.rb",
                     "lib/rubylisp/builtins.rb",
                     "lib/rubylisp/character.rb",
                     "lib/rubylisp/class_object.rb",
                     "lib/rubylisp/cons_cell.rb",
                     "lib/rubylisp/environment.rb",
                     "lib/rubylisp/environment_frame.rb",
                     "lib/rubylisp/eof_object.rb",
                     "lib/rubylisp/exception.rb",
                     "lib/rubylisp/ext.rb",
                     "lib/rubylisp/ffi_new.rb",
                     "lib/rubylisp/ffi_send.rb",
                     "lib/rubylisp/ffi_static.rb",
                     "lib/rubylisp/frame.rb",
                     "lib/rubylisp/function.rb",
                     "lib/rubylisp/macro.rb",
                     "lib/rubylisp/native_object.rb",
                     "lib/rubylisp/number.rb",
                     "lib/rubylisp/parser.rb",
                     "lib/rubylisp/port.rb",
                     "lib/rubylisp/prim_alist.rb",
                     "lib/rubylisp/prim_assignment.rb",
                     "lib/rubylisp/prim_character.rb",
                     "lib/rubylisp/prim_class_object.rb",
                     "lib/rubylisp/prim_environment.rb",
                     "lib/rubylisp/prim_equivalence.rb",
                     "lib/rubylisp/prim_frame.rb",
                     "lib/rubylisp/prim_io.rb",
                     "lib/rubylisp/prim_list_support.rb",
                     "lib/rubylisp/prim_logical.rb",
                     "lib/rubylisp/prim_math.rb",
                     "lib/rubylisp/prim_native_object.rb",
                     "lib/rubylisp/prim_relational.rb",
                     "lib/rubylisp/prim_special_forms.rb",
                     "lib/rubylisp/prim_string.rb",
                     "lib/rubylisp/prim_system.rb",
                     "lib/rubylisp/prim_type_checks.rb",
                     "lib/rubylisp/prim_vector.rb",
                     "lib/rubylisp/primitive.rb",
                     "lib/rubylisp/string.rb",
                     "lib/rubylisp/symbol.rb",
                     "lib/rubylisp/tokenizer.rb",
                     "lib/rubylisp/vector.rb",
                     "lib/rubymotion/debug.rb",
                     "lib/rubymotion/require-fix.rb",
                     "README.md"]
  gem.require_paths = ['lib']
  gem.homepage    = 'https://bitbucket.org/dastels/rubylisp'
  gem.license     = 'MIT'
end

