describe 'cond' do

  it "evals to nil with no clauses" do
    list = Lisp::Parser.new.parse("(cond)")
    expect(list.evaluate(Lisp::EnvironmentFrame.global)).to be_nil
  end

end

