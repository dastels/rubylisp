
describe "Tokenizer" do

  it "tokenizes single character symbols" do
    t = Lisp::Tokenizer.from_string('a a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'a'
  end

  it "tokenizes multiple character symbols" do
    t = Lisp::Tokenizer.from_string('abc a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with digits" do
    t = Lisp::Tokenizer.from_string('ab123c a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'ab123c'
  end

  it "tokenizes symbols with dash" do
    t = Lisp::Tokenizer.from_string('abc-def a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc-def'
  end

  it "tokenizes symbols with question mark" do
    t = Lisp::Tokenizer.from_string('abc? a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc?'
  end

  it "tokenizes symbols with exclaimation mark" do
    t = Lisp::Tokenizer.from_string('abc! a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc!'
  end

  it "tokenizes symbols with an equal sign" do
    t = Lisp::Tokenizer.from_string('abc= a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc='
  end

  it "tokenizes symbols with colon" do
    t = Lisp::Tokenizer.from_string('ab:c a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'ab:c'
  end

  it "tokenizes symbols with slash" do
    t = Lisp::Tokenizer.from_string('ab/c a')
    tok, lit = t.next_token
    expect(tok).to eq :FFI_STATIC_SYMBOL
    expect(lit).to eq 'ab/c'
  end

  it "tokenizes symbols with a leading period" do
    t = Lisp::Tokenizer.from_string('.abc a')
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with a trailing period" do
    t = Lisp::Tokenizer.from_string('abc. a')
    tok, lit = t.next_token
    expect(tok).to eq :FFI_NEW_SYMBOL
    expect(lit).to eq 'abc'
  end

  it "tokenizes symbols with a leading and trailing period" do
    t = Lisp::Tokenizer.from_string('.abc. a')
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'abc'
    t.consume_token
    tok, lit = t.next_token
    expect(tok).to eq :PERIOD
    expect(lit).to eq '.'
  end

  it "tokenizes symbols with a embedded period" do
    t = Lisp::Tokenizer.from_string('abc.a a')
    tok, lit = t.next_token
    expect(tok).to eq :SYMBOL
    expect(lit).to eq 'abc'
    t.consume_token
    tok, lit = t.next_token
    expect(tok).to eq :FFI_SEND_SYMBOL
    expect(lit).to eq 'a'
  end

  it "tokenizes short numbers" do
    t = Lisp::Tokenizer.from_string('1 a')
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '1'
  end

  it "tokenizes long numbers" do
    t = Lisp::Tokenizer.from_string('1234567 a')
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '1234567'
  end

  it "tokenizes hex numbers" do
    t = Lisp::Tokenizer.from_string('#x1f a')
    tok, lit = t.next_token
    expect(tok).to eq :HEXNUMBER
    expect(lit).to eq '1f'
  end

  it "tokenizes uppercase hex numbers" do
    t = Lisp::Tokenizer.from_string('#x1F a')
    tok, lit = t.next_token
    expect(tok).to eq :HEXNUMBER
    expect(lit).to eq '1F'
  end

  it "tokenizes floating numbers" do
    t = Lisp::Tokenizer.from_string('1.42 a')
    tok, lit = t.next_token
    expect(tok).to eq :FLOAT
    expect(lit).to eq '1.42'
  end

  it "tokenizes negative numbers" do
    t = Lisp::Tokenizer.from_string('-1 a')
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '-1'
  end

  it "tokenizes string" do
    t = Lisp::Tokenizer.from_string('"hi" a')
    tok, lit = t.next_token
    expect(tok).to eq :STRING
    expect(lit).to eq 'hi'
  end

  it "tokenizes simple character" do
    t = Lisp::Tokenizer.from_string('#\a a')
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'a'
  end

  it "tokenizes named character" do
    t = Lisp::Tokenizer.from_string('#\space a')
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'space'
  end

  it "tokenizes character code" do
    t = Lisp::Tokenizer.from_string('#\U+61 a')
    tok, lit = t.next_token
    expect(tok).to eq :CHARACTER
    expect(lit).to eq 'U+61'
  end

  it "tokenizes quote" do
    t = Lisp::Tokenizer.from_string('\'a')
    tok, lit = t.next_token
    expect(tok).to eq :QUOTE
  end

  it "tokenizes quasiquote" do
    t = Lisp::Tokenizer.from_string('`a')
    tok, lit = t.next_token
    expect(tok).to eq :BACKQUOTE
  end

  it "tokenizes unquote" do
    t = Lisp::Tokenizer.from_string(',a')
    tok, lit = t.next_token
    expect(tok).to eq :COMMA
  end

  it "tokenizes unquote-splicing" do
    t = Lisp::Tokenizer.from_string(',@a')
    tok, lit = t.next_token
    expect(tok).to eq :COMMAAT
  end

  it "tokenizes (" do
    t = Lisp::Tokenizer.from_string('( a')
    tok, lit = t.next_token
    expect(tok).to eq :LPAREN
  end

  it "tokenizes #(" do
    t = Lisp::Tokenizer.from_string('#( a')
    tok, lit = t.next_token
    expect(tok).to eq :HASH_LPAREN
  end

  it "tokenizes '#(" do
    t = Lisp::Tokenizer.from_string('\'#( a')
    tok, lit = t.next_token
    expect(tok).to eq :QUOTE_HASH_LPAREN
  end

  it "tokenizes )" do
    t = Lisp::Tokenizer.from_string(') a')
    tok, lit = t.next_token
    expect(tok).to eq :RPAREN
  end

  it "tokenizes {" do
    t = Lisp::Tokenizer.from_string('{ a')
    tok, lit = t.next_token
    expect(tok).to eq :LBRACE
  end

  it "tokenizes '{" do
    t = Lisp::Tokenizer.from_string('\'{ a')
    tok, lit = t.next_token
    expect(tok).to eq :QUOTE_LBRACE
  end

  it "tokenizes }" do
    t = Lisp::Tokenizer.from_string('} a')
    tok, lit = t.next_token
    expect(tok).to eq :RBRACE
  end

  it "tokenizes [" do
    t = Lisp::Tokenizer.from_string('[ a')
    tok, lit = t.next_token
    expect(tok).to eq :LBRACKET
  end

  it "tokenizes ]" do
    t = Lisp::Tokenizer.from_string('] a')
    tok, lit = t.next_token
    expect(tok).to eq :RBRACKET
  end

  it "tokenizes ." do
    t = Lisp::Tokenizer.from_string('. a')
    tok, lit = t.next_token
    expect(tok).to eq :PERIOD
  end

  it "tokenizes #t" do
    t = Lisp::Tokenizer.from_string('#t a')
    tok, lit = t.next_token
    expect(tok).to eq :TRUE
  end

  it "tokenizes #f" do
    t = Lisp::Tokenizer.from_string('#f a')
    tok, lit = t.next_token
    expect(tok).to eq :FALSE
  end

  it "skips comment to eof" do
    t = Lisp::Tokenizer.from_string('; a')
    tok, lit = t.next_token
    expect(tok).to eq :EOF
  end

  it "skips comment returning next token" do
    t = Lisp::Tokenizer.from_string("; a\n42 ")
    tok, lit = t.next_token
    expect(tok).to eq :NUMBER
    expect(lit).to eq '42'
  end

end
